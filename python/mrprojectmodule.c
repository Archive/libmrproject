/* -*- Mode: C; c-basic-offset: 4 -*- */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>

void initmrproject(void);
void mrproject_register_classes (PyObject *d);
void mrproject_add_constants(PyObject *module, const gchar *strip_prefix);

extern PyMethodDef mrproject_functions[];

DL_EXPORT(void)
initmrproject(void)
{
    PyObject *m, *d;
	
    init_pygobject ();

    m = Py_InitModule ("mrproject", mrproject_functions);
    d = PyModule_GetDict (m);
	
    mrproject_register_classes (d);
    mrproject_add_constants (m, "MRP_");

    if (PyErr_Occurred ()) {
	Py_FatalError ("can't initialise module mrproject");
    }
}
