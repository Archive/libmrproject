# translation of libmrproject.HEAD.nl.po to Dutch
# Libmrproject Dutch translation.
# Copyright (C) 2003 libmrproject'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Jan-Willem Harmanny <jwharmanny@hotmail.com>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: libmrproject.HEAD.nl\n"
"POT-Creation-Date: 2003-09-11 12:36+0200\n"
"PO-Revision-Date: 2003-03-14 19:15+0100\n"
"Last-Translator: Jan-Willem Harmanny <jwharmanny@hotmail.com>\n"
"Language-Team: Dutch <nl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: mrproject/mrp-project.c:441
msgid "Default"
msgstr "Standaard"

#: mrproject/mrp-project.c:749
#, c-format
msgid "Couldn't find a suitable file module for loading '%s'"
msgstr "Kan geen passende bestandsmodule vinden voor het laden van '%s'"

#: mrproject/mrp-project.c:770 mrproject/mrp-project.c:1034
msgid "No support for SQL storage built into this version of MrProject."
msgstr ""
"Er is geen ondersteuning voor SQL opslag in deze versie van MrProject "
"ingebouwd."

#: mrproject/mrp-project.c:807
msgid "Invalid URI."
msgstr "Ongeldige URI."

#: mrproject/mrp-project.c:928
#, c-format
msgid "Unable to find file writer identified by '%s'"
msgstr "Kan bestandschrijver met de naam '%s' niet vinden"

#: mrproject/mrp-project.c:991
msgid "Couldn't find a suitable file module for loading project"
msgstr "Kan geen passende bestandsmodule vinden voor het laden van dit project"

#: mrproject/mrp-property.c:365
msgid "Text"
msgstr "Tekst"

#: mrproject/mrp-property.c:367
msgid "String list"
msgstr "String lijst"

#: mrproject/mrp-property.c:369
msgid "Integer number"
msgstr "Geheel getal"

#: mrproject/mrp-property.c:371
msgid "Floating-point number"
msgstr "Drijvende-komma getal"

#: mrproject/mrp-property.c:373
msgid "Date"
msgstr "Datum"

#: mrproject/mrp-property.c:375
msgid "Duration"
msgstr "Duur"

#: mrproject/mrp-property.c:377
msgid "Cost"
msgstr "Kosten"

#: mrproject/mrp-property.c:380
msgid "None"
msgstr "Geen"

#: mrproject/mrp-task-manager.c:1769
msgid "Can not add a predecessor relation between a task and its ancestor."
msgstr ""
"Kan geen voorganger-relatie toevoegen tussen een taak en zijn voorganger."

#: mrproject/mrp-task-manager.c:1778 mrproject/mrp-task-manager.c:1786
msgid "Can not add a predecessor, because it would result in a loop."
msgstr "Kan geen voorganger toevoegen, omdat het een lus veroorzaakt."

#: mrproject/mrp-task-manager.c:1821
msgid "Can not move the task, since it would result in a loop."
msgstr "Kan de taak niet verplaatsen, omdat het een lus veroorzaakt."

#: mrproject/mrp-task.c:1011
msgid ""
"Could not add a predecessor relation, because the tasks are already related."
msgstr ""
"Kan geen voorganger-relatie toevoegen, omdat de taken al relaties hebben."

#: mrproject/mrp-day.c:54
msgid "Working"
msgstr "Werken"

#: mrproject/mrp-day.c:55
msgid "A default working day"
msgstr "Een normale werkdag"

#: mrproject/mrp-day.c:57
msgid "Nonworking"
msgstr "Niet werken"

#: mrproject/mrp-day.c:58
msgid "A default non working day"
msgstr "Een normale vrije dag"

#: mrproject/mrp-day.c:60
msgid "Use base"
msgstr "Gebruiksbasis"

#: mrproject/mrp-day.c:61
msgid "Use day from base calendar"
msgstr "Gebruik dag van basiskalender"

#: storage-modules/mrproject-1/mrp-parser.c:1043
#: storage-modules/mrproject-1/mrp-parser.c:1138
msgid "Could not create XML tree"
msgstr "Kan XML boomstructuur niet aanmaken"

#: storage-modules/mrproject-1/mrp-parser.c:1104
msgid "Could not write XML file"
msgstr "Kan XML bestand niet schrijven"

#: storage-modules/sql/mrp-sql.c:229
msgid "Invalid Unicode"
msgstr "Ongeldige Unicode"

#: storage-modules/sql/mrp-sql.c:2209
#, c-format
msgid ""
"The project '%s' has been changed by the user '%s' since you opened it. Do "
"you want to save anyway?"
msgstr ""
"Het project '%s' is veranderd door de gebruiker '%s' sinds u het hebt "
"geopend. Wilt u het toch opslaan?"

#: storage-modules/sql/mrp-sql.c:3447
#, c-format
msgid ""
"Connection to database '%s' failed.\n"
"%s"
msgstr ""
"Verbinding met database '%s' mislukt.\n"
"%s"

#: storage-modules/sql/mrp-storage-sql.c:205
msgid "Invalid SQL URI (must start with 'sql://' and contain '#')."
msgstr "Ongeldige SQL URI (moet beginnen met 'sql://' en een '#' bevatten)."

#: storage-modules/sql/mrp-storage-sql.c:274
msgid "Invalid SQL URI (invalid project id)."
msgstr "Ongeldige SQL URI (ongeldig project id)."

#: storage-modules/sql/mrp-storage-sql.c:282
msgid "Invalid SQL URI (no database name specified)."
msgstr "Ongeldige SQL URI (geen database naam gespecificeerd)."

#~ msgid ""
#~ "Could not load file.\n"
#~ "Error was: \"%s\""
#~ msgstr ""
#~ "Kan bestand niet laden.\n"
#~ "Fout was: \"%s\""

#~ msgid "No MPX header"
#~ msgstr "Geen MPX header"

#~ msgid "Not an mpx '%s'"
#~ msgstr "Niet een mpx '%s'"

#~ msgid "Jan"
#~ msgstr "Jan"

#~ msgid "Feb"
#~ msgstr "Feb"

#~ msgid "Mar"
#~ msgstr "Mrt"

#~ msgid "Apr"
#~ msgstr "Apr"

#~ msgid "May"
#~ msgstr "Mei"

#~ msgid "Jun"
#~ msgstr "Jun"

#~ msgid "Jul"
#~ msgstr "Jul"

#~ msgid "Aug"
#~ msgstr "Aug"

#~ msgid "Sep"
#~ msgstr "Sep"

#~ msgid "Oct"
#~ msgstr "Okt"

#~ msgid "Nov"
#~ msgstr "Nov"

#~ msgid "Dec"
#~ msgstr "Dec"

#~ msgid "January"
#~ msgstr "Januari"

#~ msgid "February"
#~ msgstr "Februari"

#~ msgid "March"
#~ msgstr "Maart"

#~ msgid "April"
#~ msgstr "April"

#~ msgid "June"
#~ msgstr "Juni"

#~ msgid "July"
#~ msgstr "Juli"

#~ msgid "August"
#~ msgstr "Augustus"

#~ msgid "September"
#~ msgstr "September"

#~ msgid "October"
#~ msgstr "Oktober"

#~ msgid "November"
#~ msgstr "November"

#~ msgid "December"
#~ msgstr "December"

#~ msgid "J 1"
#~ msgstr "J 1"

#~ msgid "F 2"
#~ msgstr "F 2"

#~ msgid "M 3"
#~ msgstr "M 3"

#~ msgid "A 4"
#~ msgstr "A 4"

#~ msgid "M 5"
#~ msgstr "M 5"

#~ msgid "J 6"
#~ msgstr "J 6"

#~ msgid "J 7"
#~ msgstr "J 7"

#~ msgid "A 8"
#~ msgstr "A 8"

#~ msgid "S 9"
#~ msgstr "S 9"

#~ msgid "O 10"
#~ msgstr "O 10"

#~ msgid "N 11"
#~ msgstr "N 11"

#~ msgid "D 12"
#~ msgstr "D 12"

#~ msgid "Sun"
#~ msgstr "Zo"

#~ msgid "Mon"
#~ msgstr "Ma"

#~ msgid "Tue"
#~ msgstr "Di"

#~ msgid "Wed"
#~ msgstr "Wo"

#~ msgid "Thu"
#~ msgstr "Do"

#~ msgid "Fri"
#~ msgstr "Vr"

#~ msgid "Sat"
#~ msgstr "Za"

#~ msgid "Sunday"
#~ msgstr "Zondag"

#~ msgid "Monday"
#~ msgstr "Maandag"

#~ msgid "Tuesday"
#~ msgstr "Dinsdag"

#~ msgid "Wednesday"
#~ msgstr "Woensdag"

#~ msgid "Thursday"
#~ msgstr "Donderdag"

#~ msgid "Friday"
#~ msgstr "Vrijdag"

#~ msgid "Saturday"
#~ msgstr "Zaterdag"
