/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nill; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002-2003 CodeFactory AB
 * Copyright (C) 2002-2003 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib.h>
#include <gmodule.h>
#include <libxslt/xslt.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
#include "libexslt/exslt.h"
#include <string.h>
#include <mrproject/mrp-file-module.h>
#include <mrproject/mrp-private.h>

void              init                    (MrpFileModule    *module,
					   MrpApplication   *application);
static gboolean   mrproject_html_write    (MrpFileWriter    *writer,
					   MrpProject       *project,
					   const gchar      *uri,
					   gboolean          force,
					   GError          **error);

static gboolean
mrproject_html_write (MrpFileWriter  *writer,
		      MrpProject     *project,
		      const gchar    *uri,
		      gboolean        force,
		      GError        **error)
{
	g_return_val_if_fail (writer != NULL, FALSE);
        gchar          *xml_project;
        xsltStylesheet *stylesheet;
        xmlDoc         *doc;
        xmlDoc         *final_doc;

	mrp_project_save_to_xml (project, &xml_project, NULL);

        /* libxml housekeeping */
        xmlSubstituteEntitiesDefault(1);
        xmlLoadExtDtdDefaultValue = 1;
        exsltRegisterAll ();

        stylesheet = xsltParseStylesheetFile (STYLESHEETDIR"/mrproject2html.xsl");

        doc = xmlParseMemory (xml_project, strlen (xml_project));
                                                                                
        final_doc = xsltApplyStylesheet (stylesheet, doc, NULL);
                                                                                
        xmlFree (doc);
                                                                                
        xsltSaveResultToFilename (uri, final_doc, stylesheet, FALSE);

	xsltFreeStylesheet (stylesheet);
        xmlFree (final_doc);

	return TRUE;
}
#if 0
static const gchar *
mrproject_html_get_string (MrpFileWriter *writer)
{
	return "MrProject HTML";
}

static const gchar *
mrproject_html_get_mime_type (MrpFileWriter *writer) {
	return "text/html";
}
#endif
G_MODULE_EXPORT void
init (MrpFileModule *module, MrpApplication *application)
{
        MrpFileWriter *writer;
        
        writer             = g_new0 (MrpFileWriter, 1);
        writer->module     = module;
	writer->identifier = "MrProject HTML";
	writer->mime_type  = "text/html";
        writer->priv       = NULL;
	
        writer->write      = mrproject_html_write;

        imrp_application_register_writer (application, writer);
}

