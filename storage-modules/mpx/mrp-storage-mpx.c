/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001-2002 CodeFactory AB
 * Copyright (C) 2001-2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2001-2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <gmodule.h>
#include "mrproject/mrp-error.h"
#include "mrproject/mrp-storage-module.h"
#include "mrproject/mrp-private.h"
#include "mrp-storage-mpx.h"
#include "mrp-parser.h"

static void       mpsm_init        (MrpStorageMpx       *storage);
static void       mpsm_class_init  (MrpStorageMpxClass  *class);
static gboolean   mpsm_load        (MrpStorageModule    *module,
				    const gchar         *uri,
				    GError             **error);
static void       mpsm_set_project (MrpStorageModule    *module,
				    MrpProject          *project);

void              module_init      (GTypeModule         *module);
MrpStorageModule *module_new       (void                *project);
void              module_exit      (void);


static MrpStorageModuleClass *parent_class;
GType mrp_storage_mpx_type = 0;

void
mrp_storage_mpx_register_type (GTypeModule *module)
{
	static const GTypeInfo object_info = {
		sizeof (MrpStorageMpxClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) mpsm_class_init,
		NULL,           /* class_finalize */
		NULL,           /* class_data */
		sizeof (MrpStorageMpx),
		0,              /* n_preallocs */
		(GInstanceInitFunc) mpsm_init,
	};
	
	mrp_storage_mpx_type = g_type_module_register_type (
		module,
		MRP_TYPE_STORAGE_MODULE,
		"MrpStorageMpx",
		&object_info, 0);
}

static void
mpsm_init (MrpStorageMpx *storage)
{
}

static void
mpsm_class_init (MrpStorageMpxClass *klass)
{
	MrpStorageModuleClass *mrp_storage_module_class = MRP_STORAGE_MODULE_CLASS (klass);
	
	parent_class = MRP_STORAGE_MODULE_CLASS (g_type_class_peek_parent (klass));
	
	mrp_storage_module_class->load = mpsm_load;
	mrp_storage_module_class->set_project = mpsm_set_project;
}

G_MODULE_EXPORT void
module_init (GTypeModule *module)
{
	mrp_storage_mpx_register_type (module);
}

G_MODULE_EXPORT MrpStorageModule *
module_new (void *project)
{
	MrpStorageModule *module;

	module = MRP_STORAGE_MODULE (g_object_new (MRP_TYPE_STORAGE_MPX,
						   NULL));

	return module;
}

G_MODULE_EXPORT void
module_exit (void)
{
}

static gboolean
mpsm_load (MrpStorageModule *module, const gchar *uri, GError **error)
{
	MrpStorageMpx  *sm;
	MrpTaskManager *task_manager;
	GList *node;
	
	g_print ("in load!!\n");

	sm = MRP_STORAGE_MPX (module);

	if (!mrp_parser_load (MRP_STORAGE_MPX (module), uri, error)) {
		return FALSE;
	}
	
	task_manager = imrp_project_get_task_manager (sm->project);
	
	mrp_task_manager_set_root (task_manager,
				   sm->root_task);

	imrp_project_set_resources (sm->project, sm->resources);

	g_object_set (sm->project,
		      "name", sm->name,
		      "organization", sm->organization,
		      "manager", sm->manager,
		      "project-start", &sm->project_start,
		      NULL);

	for (node = sm->assignments; node; node = node->next) {
		MrpAssignment *assignment = MRP_ASSIGNMENT (node->data);

		imrp_task_add_assignment (mrp_assignment_get_task (assignment),
					 assignment);
		imrp_resource_add_assignment (mrp_assignment_get_resource (assignment),
					     assignment);
		g_object_unref (assignment);
	}

	
	return TRUE;
}

static void
mpsm_set_project (MrpStorageModule *module,
		  MrpProject       *project)
{
	MRP_STORAGE_MPX (module)->project = project;
}
