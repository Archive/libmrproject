/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001-2002  CodeFactory AB
 * Copyright (C) 2001-2002  Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2001-2002  Mikael Hallendal <micke@codefactory.se>
 * Copyright (C) 2002       Anders Carlsson <andersca@gnu.org>
 * Copyright (C) 2001       Ximian, Inc
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "mrproject/mrp-private.h"
#include "mrproject/mrp-error.h"
#include "mrproject/mrp-intl.h"
#include "mrproject/mrp-task.h"
#include "mrproject/mrp-resource.h"
#include "mrproject/mrp-group.h"
#include "mrp-parser.h"

typedef struct {
	FILE *fh;
	
	GArray               *resource_offsets;
	GArray               *task_offsets;

	MrpTask *current_task;
	
	GArray *outlines;
	gint cur_line;

	GList *assignments;
	MrpProject     *project;
	MrpTask *root_task;
	GHashTable *task_hash;
	GHashTable *resource_hash;
	GList          *resources;
	gchar *name;
	gchar *company;
	gchar *manager;
	mrptime project_start;
} MrpParser;

static int
mpx_peek (MrpParser *parser)
{
	int c;

	c = fgetc (parser->fh);
	ungetc (c, parser->fh);

	return c;
}

static gchar *
mpx_next_token (MrpParser *parser)
{
	GString *str = g_string_sized_new (8);
	gchar   *s;
	gboolean quoted = FALSE;
	gint     c;

	do {
		c = fgetc (parser->fh);
		
		if (c == EOF) {
			break;
		}
		
		if (c == '"') {
			if (quoted) {
				if (mpx_peek (parser) == '"') {
					c = fgetc (parser->fh);
					g_string_append_c (str, c);
				} else {
					quoted = FALSE;
				}
			} else {
				quoted = TRUE;
			}
		} else {
			if ((!quoted && c == ',') ||
			    c == '\n' || c == '\r' || c == EOF) {
				break;
			} else {
				g_string_append_c (str, c);
			}
		}
	} while (1);
	
	if (str->len == 0 && (c == EOF || c == '\n' || c == '\r')) {
		
		if (c == '\n' && mpx_peek (parser) == '\r') {
			fgetc (parser->fh);
		}
		
		parser->cur_line++;
		
		g_string_free (str, TRUE);
		
		return NULL;
	}
	
//	fprintf (stderr, "mpx_next_token '%s'\n", str->str);
	
	s = str->str;
	g_string_free (str, FALSE);

	return s;
}

static gboolean
mpx_eof (MrpParser *parser)
{
	return feof (parser->fh);
}

static gboolean
mpx_next_line (MrpParser *parser)
{
	int c;

	while ((c = fgetc (parser->fh)) != EOF && c != '\n') {
		;
	}

	if (c == '\n' && mpx_peek (parser) == '\r') {
		c = fgetc (parser->fh);
	}

	parser->cur_line++;

	return c != EOF;
}

/* General field handling */

typedef void (*MpxFieldIdHandlerFn) (gpointer    ctx,
				     int         task_id, 
				     const char *tok);

typedef struct {
	const char          *name;
	int                  id;
	MpxFieldIdHandlerFn  fn;
} MpxFieldDef;

static const MpxFieldDef *
mpx_get_field_def (const MpxFieldDef *fields, int task_id)
{
	const MpxFieldDef *def;

	for (def = fields; def->id; def++) {
		if (def->id == task_id) {
			break;
		}
	}

	if (def->id == 0) {
		return NULL;
	}

	return def;
}

static void
mpx_build_field_table (MrpParser         *parser,
		       GArray            *offsets,
		       const MpxFieldDef *fields)
{
	char *tok;

	while ((tok = mpx_next_token (parser))) {
		const MpxFieldDef *def;
		guint              idx = atoi (tok);

		def = mpx_get_field_def (fields, idx);

		if (!def) {
			g_warning ("Unknown field '%s'", tok);
		}
#ifdef MPX_DEBUG
		else {
			fprintf (stderr, "Field %d = '%s'\n", 
				 offsets->len,
				 def->name);
		}
#endif

		g_array_append_val (offsets, idx);

		g_free (tok);
	}
}

static void
mpx_read_fields (MrpParser *parser,
		 const MpxFieldDef *fields,
		 GArray *offsets,
		 gpointer closure)
{
	char  *tok;
	guint  field = 0;
	
	while ((tok = mpx_next_token (parser))) {
		const MpxFieldDef *def;
		guint              idx;

		if (field >= offsets->len) {
			g_warning ("Too many fields at line %d", 
				   parser->cur_line);
			break;
		}

		idx = g_array_index (offsets, guint, field);

		def = mpx_get_field_def (fields, idx);

		if (def) {
			if (def->fn) {
				def->fn (closure, idx, tok);
			}
		} /* else - already warned: drop silently*/

		g_free (tok);
		field++;
	}
}

/* Task handling */
typedef struct {
	MrpParser *parser;

	mrptime start_time;
	mrptime finish_time;
	
	guint id;
	guint outline_level;
	guint predecessors;
	guint percent_complete;
	
	gchar *name;
} MpxTaskCtx;

enum {
	MPX_TASK_DEF_NAME             = 1,
	MPX_TASK_DEF_OUTLINE_LEVEL    = 3,
	MPX_TASK_DEF_START            = 50,
	MPX_TASK_DEF_FINISH           = 51,
	MPX_TASK_DEF_PREDECESSORS     = 70,
	MPX_TASK_DEF_ID               = 90,
	MPX_TASK_DEF_PERCENT_COMPLETE = 44
};

static void
mpx_task_handle_int (gpointer ctx, int task_id, const char *tok)
{
	MpxTaskCtx *tctx = ctx;

	switch (task_id) {
	case MPX_TASK_DEF_ID:
		tctx->id = atoi (tok);
		break;
	case MPX_TASK_DEF_OUTLINE_LEVEL:
		tctx->outline_level = atoi (tok);
		break;
	case MPX_TASK_DEF_PREDECESSORS:
		tctx->predecessors = atoi (tok);
		break;
	case MPX_TASK_DEF_PERCENT_COMPLETE:
		tctx->percent_complete = atoi (tok);
		break;
		break;
	default:
		g_error ("unknown task int %d", task_id);
	}
}

static void
mpx_task_handle_string (gpointer ctx, int task_id, const char *tok)
{
	MpxTaskCtx *tctx = ctx;

	switch (task_id) {
	case MPX_TASK_DEF_NAME:
		tctx->name = g_strdup (tok);
		break;
	default:
		g_error ("unknown task string %d", task_id);
	}
}

static void
mpx_task_handle_time (gpointer ctx, int task_id, const char *tok)
{
	MpxTaskCtx *tctx = ctx;
	
	
	switch (task_id) {
	case MPX_TASK_DEF_START:
		tctx->start_time = mrp_time_from_msdate_string (tok);
		break;
	case MPX_TASK_DEF_FINISH:
		tctx->finish_time = mrp_time_from_msdate_string (tok);
		break;
	default:
		g_error ("unknown task time %d", task_id);
	}
}

static const MpxFieldDef
mpx_task_defs [] = {
	{ "% Complete", MPX_TASK_DEF_PERCENT_COMPLETE, mpx_task_handle_int },
	{ "% Work Complete", 25, NULL },
	{ "Actual Cost", 32, NULL },
	{ "Actual Duration", 42, NULL },
	{ "Actual Finish", 59, NULL },
	{ "Actual Start", 58, NULL },
	{ "Actual Work", 22, NULL },
	{ "Baseline Cost", 31, NULL },
	{ "Baseline Duration", 41, NULL },
	{ "Baseline Finish", 57, NULL },
	{ "Baseline Start", 56, NULL },
	{ "Baseline Work", 21, NULL },
	{ "BCWP", 86, NULL },
	{ "BCWS", 85, NULL },
	{ "Confirmed", 135, NULL },
	{ "Constraint Date", 68, NULL },
	{ "Constraint Type", 91, NULL },
	{ "Contact", 15, NULL },
	{ "Cost", 30, NULL },
	{ "Cost1", 36, NULL },
	{ "Cost2", 37, NULL },
	{ "Cost3", 38, NULL },
	{ "Cost Variance", 34, NULL },
	{ "Created", 125, NULL },
	{ "Critical", 82, NULL },
	{ "CV", 88, NULL },
	{ "Delay", 92, NULL },
	{ "Duration", 40, NULL },
	{ "Duration1", 46, NULL },
	{ "Duration2", 47, NULL },
	{ "Duration3", 48, NULL },
	{ "Duration Variance", 45, NULL },
	{ "Early Finish", 53, NULL },
	{ "Early Start", 52, NULL },
	{ "Finish", MPX_TASK_DEF_FINISH, mpx_task_handle_time },
	{ "Finish1", 61, NULL },
	{ "Finish2", 63, NULL },
	{ "Finish3", 65, NULL },
	{ "Finish4", 127, NULL },
	{ "Finish5", 129, NULL },
	{ "Finish Variance", 67, NULL },
	{ "Fixed", 80, NULL },
	{ "Fixed Cost", 35, NULL },
	{ "Flag1", 110, NULL },
	{ "Flag2", 111, NULL },
	{ "Flag3", 112, NULL },
	{ "Flag4", 113, NULL },
	{ "Flag5", 114, NULL },
	{ "Flag6", 115, NULL },
	{ "Flag7", 116, NULL },
	{ "Flag8", 117, NULL },
	{ "Flag9", 118, NULL },
	{ "Flag10", 119, NULL },
	{ "Free Slack", 93, NULL },
	{ "Hide Bar", 123, NULL },
	{ "ID", MPX_TASK_DEF_ID, mpx_task_handle_int },
	{ "Late Finish", 55, NULL },
	{ "Late Start", 54, NULL },
	{ "Linked Fields", 122, NULL },
	{ "Marked", 83, NULL },
	{ "Milestone", 81, NULL },
	{ "Name", MPX_TASK_DEF_NAME, mpx_task_handle_string },
	{ "Notes", 14, NULL },
	{ "Number1", 140, NULL },
	{ "Number2", 141, NULL },
	{ "Number3", 142, NULL },
	{ "Number4", 143, NULL },
	{ "Number5", 144, NULL },
	{ "Objects", 121, NULL },
	{ "Outline Level", MPX_TASK_DEF_OUTLINE_LEVEL, mpx_task_handle_int },
	{ "Outline Number", 99, NULL },
	{ "Predecessors", MPX_TASK_DEF_PREDECESSORS, mpx_task_handle_int },
	{ "Priority", 95, NULL },
	{ "Project", 97, NULL },
	{ "Remaining Cost", 33, NULL },
	{ "Remaining Duration", 43, NULL },
	{ "Remaining Work", 23, NULL },
	{ "Resource Group", 16, NULL },
	{ "Resource Initials", 73, NULL },
	{ "Resource Names", 72, NULL },
	{ "Resume", 151, NULL },
	{ "Resume No Earlier Than", 152, NULL },
	{ "Rollup", 84, NULL },
	{ "Start", MPX_TASK_DEF_START, mpx_task_handle_time },
	{ "Start1", 60, NULL },
	{ "Start2", 62, NULL },
	{ "Start3", 64, NULL },
	{ "Start4", 126, NULL },
	{ "Start5", 128, NULL },
	{ "Start Variance", 66, NULL },
	{ "Stop", 150, NULL },
	{ "Subproject File", 96, NULL },
	{ "Successors", 71, NULL },
	{ "Summary", 120, NULL },
	{ "SV", 87, NULL },
	{ "Text1", 4, NULL },
	{ "Text2", 5, NULL },
	{ "Text3", 6, NULL },
	{ "Text4", 7, NULL },
	{ "Text5", 8, NULL },
	{ "Text6", 9, NULL },
	{ "Text7", 10, NULL },
	{ "Text8", 11, NULL },
	{ "Text9", 12, NULL },
	{ "Text10", 13, NULL },
	{ "Total Slack", 94, NULL },
	{ "Unique ID", 98, NULL },
	{ "Unique ID Predecessors", 74, NULL },
	{ "Unique ID Successors", 75, NULL },
	{ "Update Needed", 136, NULL },
	{ "WBS", 2, NULL },
	{ "Work", 20, NULL },
	{ "Work Variance", 24, NULL },
	{ NULL, 0, NULL }
};

static void
mpx_insert_task (MpxTaskCtx *tctx)
{
	MrpTask *parent_task;
	MrpTask *task;
	MrpConstraint constraint;
	gint duration;

	if (tctx->outline_level > 0 &&
	    tctx->outline_level - 1 < tctx->parser->outlines->len) {
		parent_task = g_array_index (tctx->parser->outlines,
					     MrpTask *,
					     tctx->outline_level - 1);
	}
	else {
		parent_task = tctx->parser->root_task;
	}

	duration = tctx->finish_time - tctx->start_time;

	constraint.type = MRP_CONSTRAINT_MSO;
	constraint.time = tctx->start_time;

	task = g_object_new (MRP_TYPE_TASK,
			     "name", tctx->name,
			     "duration", duration,
			     "percent-complete", CLAMP (tctx->percent_complete, 0, 100),
			     
			     NULL);
	
	imrp_task_insert_child (parent_task, -1, task);

	g_object_set (G_OBJECT (task),
		      "constraint", &constraint,
		      NULL);
	
	g_array_set_size (tctx->parser->outlines, tctx->outline_level + 1);
	
	g_hash_table_insert (tctx->parser->task_hash, GINT_TO_POINTER (tctx->id), task);
	
	g_array_index (tctx->parser->outlines,
		       MrpTask *,
		       tctx->outline_level) = task;
	

	tctx->parser->current_task = task;
}
	
static void
mpx_read_task (MrpParser *parser)
{
	MpxTaskCtx tctx;

	tctx.parser = parser;
	mpx_read_fields (parser, mpx_task_defs, parser->task_offsets, &tctx);

	mpx_insert_task (&tctx);
}

static void
mpx_read_task_notes (MrpParser *parser)
{
	gchar *notes;
	gint i;
	
	notes = mpx_next_token (parser);

	/* Convert ascii 127 to \n as per the spec */
	for (i = 0; notes[i] != 0; i++) {
		if (notes[i] == 127)
			notes[i] = '\n';
	}

	g_object_set (parser->current_task,
		      "note", notes,
		      NULL);
	g_free (notes);
}

static void
mpx_read_task_resource_assignment (MrpParser *parser)
{
	MrpResource *resource;
	MrpAssignment *assignment;
	gchar *str;
	gint resource_id;

	str = mpx_next_token (parser);
	resource_id = atoi (str);
	g_free (str);

	resource = g_hash_table_lookup (parser->resource_hash, GINT_TO_POINTER (resource_id));
	g_assert (resource != NULL);

	/* FIXME: Handle units */
	assignment = g_object_new (MRP_TYPE_ASSIGNMENT,
				   "task", parser->current_task,
				   "resource", resource,
				   NULL);
	parser->assignments = g_list_prepend (parser->assignments, assignment);
	
	mpx_next_line (parser);
}

/* Resource handling */

typedef struct {
	MrpParser      *parser;
	
	gchar       *name;
	gfloat       std_rate;
	gfloat       ovt_rate;
	guint           id;
	guint           group_id;
} MpxResourceCtx;

enum {
	MPX_RESOURCE_DEF_NAME          = 1,
	MPX_RESOURCE_DEF_CODE          = 4,
	MPX_RESOURCE_DEF_GROUP         = 3,
	MPX_RESOURCE_DEF_ID            = 40,
	MPX_RESOURCE_DEF_EMAIL         = 11,
	MPX_RESOURCE_DEF_STD_RATE      = 42,
	MPX_RESOURCE_DEF_OVT_RATE      = 43
};

static void
mpx_resource_handle_int (gpointer ctx, gint res_id, const char *tok)
{
	MpxResourceCtx *rctx = ctx;
	
	switch (res_id) {
	case MPX_RESOURCE_DEF_ID:
		rctx->id = atoi (tok);
		break;
	case MPX_RESOURCE_DEF_CODE:
		/* FIXME: Handle this */
		break;
	case MPX_RESOURCE_DEF_GROUP:
		rctx->group_id = atoi (tok);
		break;
	default:
		g_error ("unknown resource int id %d", res_id);
	}
}

static void
mpx_resource_handle_rate (gpointer ctx, gint res_id, const gchar *tok)
{
	MpxResourceCtx *rctx = ctx;

	const gchar *p;
	if (tok [0] == '$') { /* FIXME - hardcoded - other currencies ? */
		p = tok + 1;
	} else {
		p = "0";
	}

	switch (res_id) {
	case MPX_RESOURCE_DEF_STD_RATE:
		rctx->std_rate = g_strtod (p, NULL);
		break;
	case MPX_RESOURCE_DEF_OVT_RATE:
		rctx->ovt_rate = g_strtod (p, NULL);
		break;
	default:
		g_error ("unknown rate id %d", res_id);
	}
}

static void
mpx_resource_handle_string (gpointer ctx, gint res_id, const gchar *tok)
{
	MpxResourceCtx *rctx = ctx;

	switch (res_id) {
	case MPX_RESOURCE_DEF_NAME:
		rctx->name = g_strdup (tok);
		break;
	default:
		g_error ("unknown resource string id %d", res_id);
	}
}

static const MpxFieldDef
mpx_resource_defs [] = {
	{ "Work Complete", 26, NULL },
	{ "Accrue At", 45, NULL },
	{ "Actual Cost", 32, NULL },
	{ "Actual Work", 22, NULL },
	{ "Base Calendar", 48, NULL },
	{ "Baseline Cost", 31, NULL },
	{ "Baseline Work", 21, NULL },
	{ "Code", MPX_RESOURCE_DEF_CODE, mpx_resource_handle_int },
	{ "Cost", 30, NULL },
	{ "Cost Per Use", 44, NULL },
	{ "Cost Variance", 34, NULL },
	{ "Email Address", MPX_RESOURCE_DEF_EMAIL, mpx_resource_handle_string },
	{ "Group", MPX_RESOURCE_DEF_GROUP, mpx_resource_handle_int },
	{ "ID", MPX_RESOURCE_DEF_ID, mpx_resource_handle_int },
	{ "Initials", 2, NULL },
	{ "Linked Fields", 51, NULL },
	{ "Max Units", 41, NULL },
	{ "Name", MPX_RESOURCE_DEF_NAME, mpx_resource_handle_string },
	{ "Notes", 10, NULL },
	{ "Objects", 50, NULL },
	{ "Overallocated", 46, NULL },
	{ "Overtime Rate", MPX_RESOURCE_DEF_OVT_RATE , mpx_resource_handle_rate },
	{ "Overtime Work", 24, NULL },
	{ "Peak", 47, NULL },
	{ "Remaining Cost", 33, NULL },
	{ "Remaining Work", 23, NULL },
	{ "Standard Rate", MPX_RESOURCE_DEF_STD_RATE, mpx_resource_handle_rate },
	{ "Text1", 5, NULL },
	{ "Text2", 6, NULL },
	{ "Text3", 7, NULL },
	{ "Text4", 8, NULL },
	{ "Text5", 9, NULL },
	{ "Unique ID", 49, NULL },
	{ "Work", 20, NULL },
	{ "Work Variance", 25, NULL },
	{ NULL, 0, NULL }
};

static void
mpx_read_resource (MrpParser *parser)
{
	MpxResourceCtx rctx;
	MrpResource *resource;
	
	rctx.parser = parser;
	mpx_read_fields (parser, mpx_resource_defs, parser->resource_offsets, &rctx);

	resource = g_object_new (MRP_TYPE_RESOURCE,
				 "name", rctx.name,
				 NULL);
	g_hash_table_insert (rctx.parser->resource_hash, GINT_TO_POINTER (rctx.id), resource);
	
	parser->resources = g_list_prepend (parser->resources, resource);
}

static void
mpx_read_header (MrpParser *parser)
{
	gchar *calendar;
	gchar *start_date;
	
	parser->name = mpx_next_token (parser);
	parser->company = mpx_next_token (parser);
	parser->manager = mpx_next_token (parser);

	calendar = mpx_next_token (parser);
	g_free (calendar);

	start_date = mpx_next_token (parser);
	if (start_date == NULL)
		return;

	parser->project_start = mrp_time_from_msdate_string (start_date);
	g_free (start_date);
	
	mpx_next_line (parser);
}

typedef enum {
	MPX_COMMENT                     = 0,

	MPX_CURRENCY_SETTINGS           = 10,
	MPX_DEFAULT_SETTINGS            = 11,
	MPX_DATE_AND_TIME_SETTINGS      = 12,
	MPX_BASE_CALENDAR_DEFINITION    = 20,
	MPX_BASE_CALENDAR_HOURS         = 25,
	MPX_BASE_CALENDAR_EXCEPTION     = 26,
	MPX_PROJECT_HEADER              = 30,

	MPX_TEXT_RESOURCE_TABLE_DEF     = 40,
	MPX_NUMERIC_RESOURCE_TABLE_DEF  = 41,
	MPX_RESOURCE                    = 50,
	MPX_RESOURCE_NOTES              = 51,
	MPX_RESOURCE_CALENDAR_DEF       = 55,
	MPX_RESOURCE_CALENDAR_HOURS     = 56,
	MPX_RESOURCE_CALENDAR_EXCEPTION = 57,

	MPX_TEXT_TASK_TABLE_DEF         = 60,
	MPX_NUMERIC_TASK_TABLE_DEF      = 61,
	MPX_TASK                        = 70,
	MPX_TASK_NOTES                  = 71,
	MPX_TASK_RECURRING              = 72,
	MPX_TASK_RESOURCE_ASSIGNMENT    = 75,
	MPX_TASK_WORKGROUP_ASSIGNMENT   = 76,

	MPX_PROJECT_NAMES               = 80,
	MPX_DDE_OLE_CLIENT_LINKS        = 81
} MpxMainId;

static void
mpx_handle_token (MrpParser *parser, MpxMainId id)
{
	switch (id) {
	case MPX_COMMENT:
		mpx_next_line (parser);
		break;

	case MPX_TEXT_TASK_TABLE_DEF:
		break; /* ignore */

	case MPX_NUMERIC_TASK_TABLE_DEF:
		mpx_build_field_table (parser, parser->task_offsets,
				       mpx_task_defs);
		break;

	case MPX_TASK:
		mpx_read_task (parser);
		break;

	case MPX_TASK_NOTES:
		mpx_read_task_notes (parser);
		break;

	case MPX_TASK_RESOURCE_ASSIGNMENT:
		mpx_read_task_resource_assignment (parser);
		break;
		
	case MPX_TEXT_RESOURCE_TABLE_DEF:
		break; /* ignore */

	case MPX_NUMERIC_RESOURCE_TABLE_DEF:
		mpx_build_field_table (parser, parser->resource_offsets,
				       mpx_resource_defs);
		break;

	case MPX_RESOURCE:
		mpx_read_resource (parser);
		break;

	case MPX_PROJECT_HEADER:
		mpx_read_header (parser);
		break;
		
        /* Not yet implemented */
	case MPX_TASK_RECURRING:
	case MPX_TASK_WORKGROUP_ASSIGNMENT:
			
	case MPX_CURRENCY_SETTINGS:
	case MPX_DEFAULT_SETTINGS:
	case MPX_DATE_AND_TIME_SETTINGS:
	case MPX_BASE_CALENDAR_DEFINITION:
	case MPX_BASE_CALENDAR_HOURS:

	case MPX_RESOURCE_NOTES:
	case MPX_RESOURCE_CALENDAR_DEF:
	case MPX_RESOURCE_CALENDAR_HOURS:
	case MPX_RESOURCE_CALENDAR_EXCEPTION:
		mpx_next_line (parser);
		break;
	default:
		g_error ("unknown id %d", id);
	}
}

gboolean
mrp_parser_load (MrpStorageMpx *module, const gchar *uri, GError **error)
{
	MrpParser    parser;
	const gchar *filename;
	gchar       *tok;
	
	g_return_val_if_fail (MRP_IS_STORAGE_MPX (module), FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);

	if (strncmp (uri, "mpx:", 4) != 0) {
		g_warning ("Mpx format parser can only handle local files (%s).", uri);
		return FALSE;
	}

	memset (&parser, 0, sizeof (parser));

	parser.project = module->project;
	parser.root_task = mrp_task_new ();
	parser.resource_offsets = g_array_new (TRUE, TRUE, sizeof (guint));
	parser.task_offsets     = g_array_new (TRUE, TRUE, sizeof (guint));
	parser.outlines = g_array_new (TRUE, TRUE, sizeof (MrpTask *));
	parser.task_hash = g_hash_table_new (NULL, NULL);
	parser.resource_hash = g_hash_table_new (NULL, NULL);
	
	filename = uri + 4; /* Skip mpx: */
	if (strncmp (filename, "///", 3) == 0) {
		/* Skip extra // */
		filename += 2;
	}

	parser.fh = fopen (filename, "r");

	if (parser.fh == NULL) {
		/* FIXME: Add more errors here */
		switch (errno) {
		default:
			g_set_error (error,
				     MRP_ERROR,
				     MRP_ERROR_FAILED,
				     _("Could not load file.\nError was: \"%s\""),
				     g_strerror (errno));
		}

		return FALSE;
	}

	if (!(tok = mpx_next_token (&parser))) {
		g_set_error (error,
			     MRP_ERROR,
			     MRP_ERROR_FAILED,
			     _("No MPX header"));
		return FALSE;
	} else if (strcmp (tok, "MPX") != 0) {
		g_set_error (error,
			     MRP_ERROR,
			     MRP_ERROR_FAILED,
			     _("Not an mpx '%s'"), tok);
		return FALSE;
	} else {
		char *ver_str;
		char *ver; 
		char *std;

		ver_str = mpx_next_token (&parser);
		ver = mpx_next_token (&parser);
		std = mpx_next_token (&parser);

		/* FIXME: Use the correct encoding */
		
		mpx_next_line (&parser);
	}

	do {
		char *str;
		int   id;
		
		str = mpx_next_token (&parser);
		if (!str) {/* rather a short line */
			continue;
		}
		
		id = atoi (str);
		g_free (str);
		
		mpx_handle_token (&parser, id);
		
	} while (!mpx_eof (&parser));

	module->resources = g_list_reverse (parser.resources);
	module->assignments = parser.assignments;
	module->project_start = parser.project_start;
	module->name = parser.name;
	module->organization = parser.company;
	module->manager = parser.manager;
	module->root_task = parser.root_task;

	return TRUE;
}
