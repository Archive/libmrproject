/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* Note: Do not include this inside libmrproject. */

#ifndef __MRPROJECT_H__
#define __MRPROJECT_H__

#include <mrproject/mrp-application.h>
#include <mrproject/mrp-error.h>
#include <mrproject/mrp-types.h>
#include <mrproject/mrp-time.h>
#include <mrproject/mrp-assignment.h>
#include <mrproject/mrp-group.h>
#include <mrproject/mrp-object.h>
#include <mrproject/mrp-project.h>
#include <mrproject/mrp-resource.h>
#include <mrproject/mrp-task.h>

#endif /* __MRPROJECT_H__ */
