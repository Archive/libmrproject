/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <string.h>
#include <gsf/gsf-input.h>

#include "mrp-intl.h"
#include "mrp-file-module.h"

static MrpFileModule *
file_module_load (const gchar *file)
{
	MrpFileModule *module;
	
	module = mrp_file_module_new ();
	
	module->handle = g_module_open (file, G_MODULE_BIND_LAZY);
	
	if (module->handle == NULL) {
		g_warning ("Could not open file module '%s'\n",
			   g_module_error ());
		
		return NULL;
	}
	
	g_module_symbol (module->handle, "init", (gpointer*)&module->init);

	return module;
}

void
mrp_file_module_load_all (MrpApplication *app)
{
	GDir*           dir;
	const gchar    *name;
	MrpFileModule  *module;

	dir = g_dir_open (MRP_FILE_MODULES_DIR, 0, NULL);

	if (dir == NULL) {		
		return;
	}

	while ((name = g_dir_read_name (dir)) != NULL) {
		if (strncmp (name + strlen (name) - 3, ".so", 3) == 0) {

			gchar *plugin = g_build_path (G_DIR_SEPARATOR_S,
						      MRP_FILE_MODULES_DIR,
						      name,
						      NULL);

			module = file_module_load (plugin);
			if (module) {
				mrp_file_module_init (module, app);
			}

			g_free (plugin);
		}
	}

	g_dir_close (dir);
}

MrpFileModule *
mrp_file_module_new (void)
{
        return g_new0 (MrpFileModule, 1);
}

void
mrp_file_module_init (MrpFileModule *plugin, MrpApplication *app)
{
        g_return_if_fail (plugin != NULL);
        g_return_if_fail (MRP_IS_APPLICATION (app));
        
        plugin->app = app;
        
        if (plugin->init) {
                plugin->init (plugin, app);
        }
}

gboolean
mrp_file_reader_read (MrpFileReader  *reader,
		      GsfInput       *input,
		      MrpProject     *project,
		      GError        **error)
{
	if (reader->read) {
		return reader->read (reader, input, project, error);
	}
	
	/* FIXME: Set error */
	return FALSE;
}

gboolean
mrp_file_reader_read_string (MrpFileReader  *reader,
			     const gchar    *str,
			     MrpProject     *project,
			     GError        **error)
{
	if (reader->read_string) {
		return reader->read_string (reader, str, project, error);
	}
	
	/* FIXME: Set error */
	return FALSE;
}

const gchar *
mrp_file_writer_get_string (MrpFileWriter *writer)
{
        g_return_val_if_fail (writer != NULL, NULL);

	if (writer->get_mime_type) {
		return writer->get_mime_type (writer);
	}

	return NULL;
}

const gchar *
mrp_file_writer_get_mime_type (MrpFileWriter *writer)
{
        g_return_val_if_fail (writer != NULL, NULL);

	if (writer->get_string) {
		return writer->get_string (writer);
	}

	return NULL;
}

gboolean
mrp_file_writer_write (MrpFileWriter    *writer, 
		       MrpProject       *project, 
		       const gchar      *uri,
		       gboolean          force,
		       GError          **error)
{
        g_return_val_if_fail (writer != NULL, FALSE);
		
        if (writer->write) {
		return writer->write (writer, project, uri, force, error);
        }

        return FALSE;
}
