#include "icaltime.h"

#define FALSE 0
#define TRUE 1

typedef struct icaltimetype mrptime;

int
main (int argc, char *argv[])
{
	mrptime t;
	char buf[128], *str;

	t = icaltime_from_timet (time (NULL), FALSE);
	str = icaltime_as_ical_string (t);
	printf ("%s, UTC: %d\n", str, t.is_utc);

	t = icaltime_from_timet (time (NULL), TRUE);
	str = icaltime_as_ical_string (t);
	printf ("%s, UTC: %d\n", str, t.is_utc);
	
	return 0;
}

